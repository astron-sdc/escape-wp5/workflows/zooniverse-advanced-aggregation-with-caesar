{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "HrVPzm6KzWma"
   },
   "source": [
    "# Zooniverse - Advanced Aggregation with _Caesar_: Working with Data\n",
    "\n",
    "Zooniverse's _Caesar_ advanced retirement and aggregation engine allows for the setup of more advanced rules for retiring subjects, and provides a powerful way of aggregating volunteer classifications (collecting the results for analysis). Aggregation involves the use of \"extractors\" and \"reducers\": extractors extract the data from each classification into a more useful data format, while reducers \"reduce\" (aggregate) the extracted data for each subject together.\n",
    "\n",
    "In this notebook, we will see how to set up, configure, and run external extractors and reducers. As an example, we will focus on clustering point data from the _Penguin Watch_ Zooniverse project, with results plotted in the `plotting_fuctions.ipynb` notebook.\n",
    "\n",
    "For this tutorial we will be working with the Zooniverse's data aggregation code `panoptes_aggregation` (_Panoptes_ is the platform behind Zooniverse). This code is designed to work directly with the data exported from a project's `Data Export` page within the Zooniverse's Project Builder.\n",
    "\n",
    "General documentation for the aggregation code can be found on https://aggregation-caesar.zooniverse.org/docs\n",
    "\n",
    "## Table of Contents\n",
    "\n",
    "1. [Setup](#Setup)\n",
    "2. [Zooniverse data exports](#Zooniverse-data-exports)\n",
    "3. [How the aggregation code works](#How-the-aggregation-code-works)\n",
    "4. [Processing the example Penguin Watch data](#Processing-the-example-Penguin-Watch-data)\n",
    "    1. [Extractor](#Edit-the-extractor-config-file)\n",
    "    2. [Reducer](#Edit-the-reducer-config-files)\n",
    "    3. [Plot the results](#Plot-the-results)\n",
    "    4. [Understanding what the files contain](#Understanding-what-the-extraction-and-reduction-files-contain)\n",
    "5. [Other things to try](#Other-things-to-try)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "HrVPzm6KzWma"
   },
   "source": [
    "## Setup\n",
    "\n",
    "You may need to install the `panoptes_aggregation` and `pandas` packages. If you do, then run the code in the next cell. Otherwise, skip it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "HrVPzm6KzWma"
   },
   "outputs": [],
   "source": [
    "!python -m pip install panoptes_aggregation\n",
    "!python -m pip install pandas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "HrVPzm6KzWma"
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import getpass\n",
    "import glob\n",
    "import os\n",
    "import io"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "RYa1sVVzyA8m"
   },
   "source": [
    "## Zooniverse data exports\n",
    "\n",
    "Zooniverse projects provide a large amount of data to research teams. The following data can be exported from the `Data Export` tab on a project's `Lab` page.\n",
    "\n",
    "### Classification export\n",
    "\n",
    "This `csv` file has one row for every classification submitted for a project. This files has the following columns:\n",
    "\n",
    "* `classification_id`: A unique ID number assigned to each classification\n",
    "* `user_name`: The name of the user that submitted the classification. Non logged-in users are assigned a unique name based on (a hashed version of) their IP address.\n",
    "* `user_id`: User ID number is provided for logged-in users\n",
    "* `user_ip`: A hashed version of the user's IP address (original IP addresses are not provided for privacy reasons)\n",
    "* `workflow_id`: The ID number for the workflow the classification was made on\n",
    "* `workflow_name`: The name of the workflow\n",
    "* `workflow_version`: The major and minor workflow version for the classification\n",
    "* `created_at`: The `UTC` timestamp for the classification\n",
    "* `gold_standard`: Identifies if the classification was made on a gold standard subject\n",
    "* `expert`: Identifies if the classification was made in \"expert\" mode\n",
    "* `metadata`: A `JSON` blob containing additional metadata about the classification (e.g. browser size, browser user agent, classification duration, etc...)\n",
    "* `annotations`: A `JSON` blob with the annotations made for each task in the workflow. The exact shape of this blob is dependent on the shape of the workflow.\n",
    "* `subject_data`: A `JSON` blob with the metadata associated with the subject that was classified. The exact shape of this blob is dependent on the metadata uploaded to each subject\n",
    "* `subject_ids`: The ID number for the subject classified\n",
    "\n",
    "### Subject export\n",
    "\n",
    "This `csv` file has one row for every subject uploaded to a project. This file has the following columns:\n",
    "\n",
    "* `subject_id`: A unique ID number assigned to each subject as they are uploaded\n",
    "* `project_id`: The ID number for the project\n",
    "* `workflow_id`: The workflow ID the subject is associated with\n",
    "* `subject_set_id`: The ID of the subject set the subject is connected to\n",
    "* `metadata`: A `JSON` blob with the subject's metadata\n",
    "* `locations`: A `JSON` blob with the URL to each `frame` of the subject\n",
    "* `classifications_count`: How many volunteers have classified the subject\n",
    "* `retired_at`: If the subject is retired this is the `UTC` timestamp for when it was retired\n",
    "* `retirement_reason`: The reason why it was retired\n",
    "\n",
    "### Workflows export\n",
    "\n",
    "This `csv` file has the information for every major version of a workflow. This file has the following columns:\n",
    "\n",
    "* `workflow_id`: The ID number for the workflow\n",
    "* `display_name`: The display name for the workflow\n",
    "* `version`: The major version number (changes when a task is edited on the workflow)\n",
    "* `active`: `true` if the workflow is active\n",
    "* `classifications_count`: How many classifications have been made on the workflow\n",
    "* `primary_language`: The language code for the workflow\n",
    "* `first_task`: The task key for the first task\n",
    "* `retired_set_member_subjects_count`: The number of retired subjects from the workflow\n",
    "* `tasks`: A `JSON` blob showing the full workflow structure\n",
    "* `retirement`: The retirement rules for the workflow\n",
    "* `strings`: A `JSON` blob containing all the text associated with the workflow\n",
    "* `minor_version`: The minor workflow version number (changes when text is edited on the workflow)\n",
    "\n",
    "All other columns are not typically used and are for experimental or more advanced workflow setups."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "RYa1sVVzyA8m"
   },
   "source": [
    "## How the aggregation code works\n",
    "\n",
    "Aggregation is done in a three step process:\n",
    "\n",
    "1. Configure `extractors` and `reducers`\n",
    "    1. Check over configuration files and edit them as needed\n",
    "2. `Extract` the data from each classification into a more useful data format (i.e. flatten and normalize the data)\n",
    "3. `Reduce` the extracts for each subject together (i.e. aggregate the data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "RYa1sVVzyA8m"
   },
   "source": [
    "## Processing the example Penguin Watch data\n",
    "\n",
    "This tutorial makes use of example material (subjects, metadata, classifications) from the [_Penguin Watch_](https://www.zooniverse.org/projects/penguintom79/penguin-watch) Zooniverse project, which involves counting the numbers of penguin adults, chicks and eggs in images to help understand their lives and environment. Volunteers are provided with marker tools to place points over each of the above categories (as well as an 'other' category), using a different marker tool for each category, often resulting in the positions of multiple points being stored for each image (point data). In addition to this point marking task, there is also a question asked to volunteers, as well as a \"shortcut\" checkbox (see below).\n",
    "\n",
    "A selection of each of the above export files for the Penguin Watch project has been provided in the `data` folder.\n",
    "\n",
    "You should make a new folder called `aggregation_results` to direct the output of the following scripts in this tutorial. These steps outline how to use the command line to run all of the aggregation scripts.\n",
    "\n",
    "For more detailed breakdowns of the commands used in this tutorial, see the [`panoptes_aggregation` documentation](https://aggregation-caesar.zooniverse.org/Scripts.html).\n",
    "\n",
    "### Run configuration script\n",
    "\n",
    "The `panoptes_aggregation` command contains three sub-commands: `config`, `extract`, and `reduce`:\n",
    "- `config` creates configuration files for _panoptes_ data extraction and reduction based on a workflow export\n",
    "- `extract` extracts data from _panoptes_ classifications based on the workflow\n",
    "- `reduce` reduces data from panoptes classifications based on the extracted data\n",
    "\n",
    "To begin, we shall use `config` to generate the configuration files we'll need.\n",
    "\n",
    "The auto-config script will detect the shape of a project's workflow and select the default extractor and reducers to use. For this example, the `penguin-watch-workflows.csv` file contains data for multiple Penguin Watch workflows and for multiple versions of these workflows, so for now let's focus on generating configuration files for workflow `6465` version `52.76`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "id": "E6mLcIouyA8n"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Saving Extractor config to:\n",
      "/home/james/git_projects/zooniverse-advanced-aggregation-with-caesar/aggregation_results/Extractor_config_workflow_6465_V52.76.yaml\n",
      "\n",
      "Saving Reducer config to:\n",
      "/home/james/git_projects/zooniverse-advanced-aggregation-with-caesar/aggregation_results/Reducer_config_workflow_6465_V52.76_point_extractor_by_frame.yaml\n",
      "\n",
      "Saving Reducer config to:\n",
      "/home/james/git_projects/zooniverse-advanced-aggregation-with-caesar/aggregation_results/Reducer_config_workflow_6465_V52.76_question_extractor.yaml\n",
      "\n",
      "Saving Reducer config to:\n",
      "/home/james/git_projects/zooniverse-advanced-aggregation-with-caesar/aggregation_results/Reducer_config_workflow_6465_V52.76_shortcut_extractor.yaml\n",
      "\n",
      "Saving task key look up table to:\n",
      "/home/james/git_projects/zooniverse-advanced-aggregation-with-caesar/aggregation_results/Task_labels_workflow_6465_V52.76.yaml\n"
     ]
    }
   ],
   "source": [
    "!panoptes_aggregation config data/penguin-watch-workflows.csv 6465 -v 52.76 -d aggregation_results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Z_PSnDnbyA8p"
   },
   "source": [
    "See `!panoptes_aggregation config -h` for help text explaining each of these inputs.\n",
    "\n",
    "This will create five new files (that you can open with a text editor):\n",
    "\n",
    "* `Extractor_config_workflow_6465_V52.yaml`: The configuration file for the extractors\n",
    "* `Reducer_config_workflow_6465_V52.76_shortcut_extractor.yaml`: The configuration file for the shortcut reducer\n",
    "* `Reducer_config_workflow_6465_V52_question_extractor.yaml`: The configuration file for the question reducer\n",
    "* `Reducer_config_workflow_6465_V52_point_extractor_by_frame.yaml`: The configuration file for the point reducer\n",
    "* `Task_labels_workflow_6465_V52.76.yaml`: A file with a look up table that matches the workflow task keys with the text associated with them"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "EgQe6CeuyA8p",
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "### Edit the extractor config file\n",
    "\n",
    "Next, we'll use the `extract` sub-command to extract data based on the extractor configuration file after we've had a look at it.\n",
    "\n",
    "In the extractor config file, there are a number of extractors listed:\n",
    "1. The point extractor `T0` corresponds to the task involving marking the penguins, whose tools `0`, `1`, `2`, and `3` correspond to marker tools for \"Adults\", \"Chicks\", \"Eggs\", and \"Other\", respectively.\n",
    "2. The question extractor `T1` corresponds to the question \"Have you marked all the animals?\" asked after marking points.\n",
    "3. The shortcut extractor `T2` corresponds to the \"This image is too dark or blurry\" checkbox that allows volunteers to skip that image.\n",
    "\n",
    "Task `T4` was never used in the final project so **it can be removed from the config file**. Today we are not interested in task `T0` tool `3` (\"Other\") so **we will remove it and its subtask from the config file**. The final file should look like:\n",
    "\n",
    "```yaml\n",
    "extractor_config:\n",
    "    point_extractor_by_frame:\n",
    "    -   task: T0\n",
    "        tools:\n",
    "        - 0\n",
    "        - 1\n",
    "        - 2\n",
    "    question_extractor:\n",
    "    -   task: T1\n",
    "    shortcut_extractor:\n",
    "    -   task: T6\n",
    "workflow_id: 6465\n",
    "workflow_version: '52.76'\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "EgQe6CeuyA8p",
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "#### Side Note: Converting this extractor config for use in _Caesar_\n",
    "\n",
    "If you have a config file that you would like converting into an extractor for use in the _Caesar_ web interface, it is worth noting that all of the extractors available within `panoptes_aggregation` can be used as \"external extractors\" in _Caesar_. The following demonstrates how to set this up, though such an external extractor won't be used in this tutorial.\n",
    "\n",
    "Note: For question tasks you will typically want to use the built-in question extractor.\n",
    "\n",
    "To begin, if you have not done so already, add a workflow to _Caesar_ in order to create an extractor: log in to the [_Caesar_ web interface](https://caesar.zooniverse.org/) using your Zooniverse account, go to the \"Workflow\" or \"Project\" tab and add yours by clicking `+Add` (IDs can be found in the Project Builder), then select a workflow.\n",
    "\n",
    "1. On the \"Extractors\" tab in _Caesar_ click \"Create Extractor\" and select \"External\".\n",
    "2. Enter a unique \"key\" (e.g. `advanced_aggregation_example`) to reference this extractor later. This key will be used in the reducer later and will show up in the `Data Export`.\n",
    "3. Enter the \"URL\" as `https://aggregation-caesar.zooniverse.org/extractors/<extractor name>?task=<task ID>`\n",
    "    - The `<task ID>` value is found next to the `task:` key for each extractor in the config file.\n",
    "    - For our example this would be written as\n",
    "    ```\n",
    "    https://aggregation-caesar.zooniverse.org/extractors/point_extractor_by_frame?task=T0&tools=[0,1,2]\n",
    "    ```\n",
    "4. Enter the \"Minimum workflow version\".\n",
    "    - All version above this number will be passed through this extractor.\n",
    "    - For our example this would be `52.76` (the value next to `workflow_version:`).\n",
    "5. Click \"Create External extractor\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the extractors\n",
    "\n",
    "The extraction script will create one `csv` file for each type of extractor being used.  In this case there will be three files created, one each for `point_extractor_by_frame`, `question_extractor` and `shortcut_extractor`.\n",
    "\n",
    "See `!panoptes_aggregation extract -h` for help text explaining each of these inputs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "id": "rHXYi2NDyA8p"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Extracting: 100% |#############################################| Time:  0:00:00\n"
     ]
    }
   ],
   "source": [
    "!panoptes_aggregation extract data/penguin-watch-classifications-trim.csv aggregation_results/Extractor_config_workflow_6465_V52.76.yaml -o example -d aggregation_results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "9a-NpnFhyA8q"
   },
   "source": [
    "### Edit the reducer config files\n",
    "\n",
    "Now, we'll use the `reduce` sub-command to reduce data based on the extracted data and and the reducer configuration files, once again after we've had a look at them.\n",
    "\n",
    "Thankfully, there are no configuration parameters for the question reducer or shortcut reducer so those files do not need to be edited.\n",
    "\n",
    "For the point reducer (`Reducer_config_workflow_6465_V52.76_point_extractor_by_frame.yaml` for this example), by default the reducer uses a clustering algorithm `DBSCAN` to aggregate the data, but we should **edit the file to switch it from the default `DBSCAN` reducer to a `HDBSCAN` one**. We are making this switch since the Penguin Watch subjects have a large depth-of-field that causes the clusters of points to be different densities across the image, which `HDBSCAN` can account for.\n",
    "\n",
    "As before, we are not interested in task `T0` tool `3` (\"Other\") so we will **remove it from the reducer config file** as well, i.e. removing the following:\n",
    "```\n",
    "details:\n",
    "    T0_tool3:\n",
    "    - question_reducer\n",
    "```\n",
    "\n",
    "We can also use this config file to **set the `min_cluster_size` and `min_samples` keywords**.  Here are some good values to start with:\n",
    "\n",
    "```yaml\n",
    "reducer_config:\n",
    "    point_reducer_hdbscan:\n",
    "        min_cluster_size: 4\n",
    "        min_samples: 3\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "9a-NpnFhyA8q"
   },
   "source": [
    "#### Side Note: Converting the reducer config for use in Caesar\n",
    "\n",
    "Continuing on from the previous side note, we can also convert our reducer config file for use in the _Caesar_ web interface. Like before, all of the reducers available within `panoptes_aggregation` can be used as \"external reducers\" in _Caesar_.\n",
    "\n",
    "Note: For question tasks you will typically want to use the built-in stats reducer.\n",
    "\n",
    "After selecting a workflow in the [_Caesar_ web interface](https://caesar.zooniverse.org/):\n",
    "\n",
    "1. On the \"Reducers\" tab in _Caesar_ click \"Create Reducer\" and select \"External\".\n",
    "2. Enter a unique \"key\" to reference this reducer later. This key is used by the rules later and will show up in the `Data Export`.\n",
    "3. Enter the \"URL\" as `https://aggregation-caesar.zooniverse.org/reducers/<reducer name>?<param 1>=<value 1>&<param 2>=<value 2>&<etc ...>`\n",
    "    - For our example this would be written as\n",
    "    ```\n",
    "    https://aggregation-caesar.zooniverse.org/reducers/point_reducer_hdbscan?min_cluster_size=4&min_samples=3\n",
    "    ```\n",
    "4. Expand the \"Filters\" section\n",
    "5. Fill in the \"Extractor keys\" section as a `list`: this is the key picked in step 2 of converting the extractor config above.\n",
    "    - Following the example above, this would be `[\"advanced_aggregation_example\"]`.\n",
    "6. Pick how you want \"Repeated Classifications\" to be handled. The default is \"keep first\", though \"keep all\" can be useful at the testing/debugging stage.\n",
    "7. Click \"Create External reducer\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the reducers\n",
    "See `panoptes_aggregation reduce -h` for help text explaining each of these inputs.\n",
    "\n",
    "Note: By default, if a volunteer classifies the same subject multiple times only the first one is used. This can be changed with the `-F` flag on the command line (e.g.  `-F all` to keep all, `-F first` to keep first, `-F last` to keep last)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "id": "wnL-m82oyA8q"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reducing: 100% |###############################################| Time:  0:00:00\n"
     ]
    }
   ],
   "source": [
    "!panoptes_aggregation reduce aggregation_results/question_extractor_example.csv aggregation_results/Reducer_config_workflow_6465_V52.76_question_extractor.yaml -o example -d aggregation_results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "id": "iw991ZbxyA8r"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reducing: 100% |###############################################| Time:  0:00:00\n"
     ]
    }
   ],
   "source": [
    "!panoptes_aggregation reduce aggregation_results/shortcut_extractor_example.csv aggregation_results/Reducer_config_workflow_6465_V52.76_shortcut_extractor.yaml -o example -d aggregation_results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "id": "5mxjVTSeyA8r"
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Reducing: 100% |###############################################| Time:  0:00:00\n"
     ]
    }
   ],
   "source": [
    "!panoptes_aggregation reduce aggregation_results/point_extractor_by_frame_example.csv aggregation_results/Reducer_config_workflow_6465_V52.76_point_extractor_by_frame.yaml -o example -d aggregation_results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "LD96TM4qyA8r"
   },
   "source": [
    "### Plot the results\n",
    "\n",
    "The final step is examining the results of the point clustering. The notebook `plotting_functions.ipynb` is included in the `notebooks` folder for this purpose.\n",
    "\n",
    "### Understanding what the extraction and reduction files contain\n",
    "\n",
    "The columns of the question extractor and question reducer files contain some data already described above and the counts for each of the possible answers for each question.\n",
    "\n",
    "#### Point extractor\n",
    "\n",
    "There are `x` and `y` data for each of the four point drawing tools:\n",
    "\n",
    "* `data.frame0.T0_tool*_x`: A list of the `x` data for each point created for `tool*`\n",
    "* `data.frame0.T0_tool*_y`: A list of the `y` data for each point created for `tool*`\n",
    "\n",
    "#### Point reducer\n",
    "\n",
    "In addition to the original point data:\n",
    "\n",
    "* `data.frame0.T0_tool*_points_x` : A list of `x` positions for **all** points drawn with `tool*`\n",
    "* `data.frame0.T0_tool*_points_y` : A list of `y` positions for **all** points drawn with `tool*`\n",
    "* `data.frame0.T0_tool*_cluster_labels` : A list of cluster labels for **all** points drawn with `tool*`\n",
    "* `data.frame0.T0_tool*_cluster_probabilities`: A list of cluster probabilities for **all** points drawn with `tool*`\n",
    "* `data.frame0.T0_tool*_clusters_persistance`: A measure for how persistent each **cluster** is (1.0 = stable, 0.0 = unstable)\n",
    "* `data.frame0.T0_tool*_clusters_count` : The number of points in each **cluster** found\n",
    "* `data.frame0.T0_tool*_clusters_x` : The weighted `x` position for each **cluster** found\n",
    "* `data.frame0.T0_tool*_clusters_y` : The weighted `y` position for each **cluster** found\n",
    "* `data.frame0.T0_tool*_clusters_var_x` : The weighted `x` variance of points in each **cluster** found\n",
    "* `data.frame0.T0_tool*_clusters_var_y` : The weighted `y` variance of points in each **cluster** found\n",
    "* `data.frame0.T0_tool*_clusters_var_x_y` : The weighted `x-y` covariance of points in each **cluster** found"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "LD96TM4qyA8r"
   },
   "source": [
    "## Other things to try\n",
    "\n",
    "* Play around with changing the `min_cluster_size` and `min_samples` parameters to see how they change the detected clusters (visualising the results in the `plotting_functions.ipynb` notebook).\n",
    "* Read the various `csv` files into you favourite programming language and explore the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "J37wu1psyA8s"
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "colab": {
   "collapsed_sections": [],
   "name": "Working_with_data.ipynb",
   "provenance": []
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
