## Zooniverse - Advanced Aggregation with Caesar

This directory contains resources for the _Advanced Aggregation with Caesar_ tutorial. This tutorial forms part of a series of advanced guides for managing Zoonivere projects through Python. While they can be done independently, for best usage you may want to complete them in the following order (these are also all available as Interactive Analysis workflows in the ESAP GUI):
1. [Advanced Project Building](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-project-building)
2. [Advanced Aggregation with Caesar (current)](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-aggregation-with-caesar)
3. [Integrating Machine Learning](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-integrating-machine-learning)

Zooniverse's _Caesar_ advanced retirement and aggregation engine allows for the setup of more advanced rules for retiring subjects (as opposed to the default method of retiring after that subject has been classified a certain number of times). _Caesar_ also provides a powerful way of collecting and analysing volunteer classifications (aggregation).

For guides on creating a Zooniverse project through the web interface or by using Python, take a look at the _Advanced Project Building_ tutorial above and the links therein. For an introduction to _Caesar_ and using its [web interface](https://caesar.zooniverse.org), see the [Zooniverse _Caesar_ help guide](https://help.zooniverse.org/next-steps/caesar-realtime-data-processing/). A [recorded introduction](https://youtu.be/zJJjz5OEUAw?t=10830) and [accompanying slides](https://indico.in2p3.fr/event/21939/contributions/89043/) are available as part of the [First ESCAPE Citizen Science Workshop](https://indico.in2p3.fr/event/21939/).

The advanced tutorial presented here include two notebooks. The first, `Working_with_data.ipynb`, demonstrates using Python for:
* Details of classification, subject, and workflow exports from Zooniverse
* Configuring aggregation extractors and reducers
* Running extractors to extract the data from each classification into a more useful data format
* Running reducers to aggregate the data

The second, `plotting_functions.ipynb`, demonstrates plotting example cluster data produced by the Zooniverse's point reducer code to visualise aggregation results. You can find the codes for the tutorial in the `notebooks` folder and the data that were used in the `data` folder.

This tutorial makes use of example material (subjects, metadata, classifications) from the [_Penguin Watch_](https://www.zooniverse.org/projects/penguintom79/penguin-watch) Zooniverse project, which involves counting the numbers of penguin adults, chicks and eggs in images to help understand their lives and environment.

A recorded walkthrough of this advanced tutorial is available [here](https://youtu.be/o9SzgsZvOCg?t=3840).

The ESAP Archives (accessible via the ESAP GUI) include data retrieval from the Zooniverse Classification Database using the ESAP Shopping Basket. For a tutorial on loading Zooniverse data from a saved shopping basket into a notebook and performing simple aggregation of the classification results, see [here](https://git.astron.nl/astron-sdc/escape-wp5/workflows/muon-hunters-example/-/tree/master) (also available as an Interactive Analyis workflow).

### Setup

#### Option 1: ESAP workflow as a remote notebook instance

You may need to install the `panoptes_aggregation` and `pandas` packages.
```
!python -m pip install panoptes_aggregation
!python -m pip install pandas
```

#### Option 2: Local computer

1. Install Python 3: the easiest way to do this is to download the Anaconda build from https://www.anaconda.com/download/. This will pre-install many of the packages needed for the aggregation code.
2. Open a terminal and run: `pip install panoptes_aggregation`
3. Download the [Advanced Aggregation with Caesar](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-aggregation-with-caesar) tutorial into a suitable directory.

#### Option 3: Google Colab

Google Colab is a service that runs Python code in the cloud.

1. Sign into Google Drive.
2. Make a copy of the [Advanced Aggregation with Caesar](https://git.astron.nl/astron-sdc/escape-wp5/workflows/zooniverse-advanced-aggregation-with-caesar) tutorial in your own Google Drive.
3. Right click the `Working_with_data.ipynb` file > Open with > Google Colaboratory.
    1. If this is not an option click "Connect more apps", search for "Google Colaboratory", enable it, and refresh the page.
4. Run the following in the notebook:
    1. `!pip install panoptes_aggregation --quiet` to install the needed packages (it will take a few minutes to finish), 
    2. `from google.colab import drive; drive.mount('/content/drive')` to mount Google Drive, and 
    3. `import os; os.chdir('/content/drive/MyDrive/zooniverse-advanced-aggregation-with-caesar/')` to change the current working directory to the example folder (adjust if you have renamed the example folder).

### Other Useful Resources

Here is a list of additional resources that you may find useful when building your own Zooniverse citizen science project.
* [_Zooniverse_ website](http://zooniverse.org) - Interested in Citizen Science? Create a **free** _Zooniverse_ account, browse other projects for inspiration, contribute yourself as a citizen scientist, and build your own project.
* [Zooniverse project builder help pages](https://help.zooniverse.org) - A great resource with practical guidance, tips and advice for building great Citizen Science projects. See the ["Building a project using the project builder"](https://youtu.be/zJJjz5OEUAw?t=7633) recorded tutorial for more information.
* [_Caesar_ web interface](https://caesar.zooniverse.org) - An online interface for the _Caesar_ advanced retirement and aggregation engine. See the ["Introducing Caesar"](https://youtu.be/zJJjz5OEUAw?t=10830) recorded tutorial for tips and advice on how to use Caesar to supercharge your _Zooniverse_ project.
* [The `panoptes_client` documentation](https://panoptes-python-client.readthedocs.io/en/v1.1/) - A comprehensive reference for the Panoptes Python Client.
* [The `panoptes_aggregation` documentation](https://aggregation-caesar.zooniverse.org/docs) - A comprehensive reference for the Panoptes Aggregation tool.
* [The `aggregation-for-caesar` GitHub](https://github.com/zooniverse/aggregation-for-caesar) - A collection of external reducers for _Caesar_ and offline use.